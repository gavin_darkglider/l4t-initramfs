# L4T-Initramfs

L4T initramfs files and build scripts.

## Building

```sh
sudo apt-get install cpio u-boot-tools
```

```sh
cd l4t-initramfs
```

```sh
./build.sh
```
