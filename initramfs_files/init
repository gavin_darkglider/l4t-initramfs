#!/bin/bash
DATE=$(date +"%T.%3N");
build_date=$(date);
export initrd_dir=/mnt/initrd;
dhclient_flag="false";
count=0;

if [ ! "$(cat /proc/cmdline | grep runlevel)" = "" ]; then
	runlevel=$(sed -ne 's/.*runlevel=//;T' -e 's/\s.*$//p' /proc/cmdline)
else
	runlevel=2
fi

source init_helpers

# Mount procfs, devfs, sysfs and debugfs
mount -t proc proc /proc
if [ $? -ne 0 ]; then
	echo "[ $(date +"%T.%3N") ] ERROR: mounting proc fail..." > /dev/kmsg
	exec -c /bin/bash
fi
mount -t devtmpfs none /dev
if [ $? -ne 0 ]; then
	echo "[ $(date +"%T.%3N") ] ERROR: mounting dev fail..." > /dev/kmsg
	exec -c /bin/bash
fi
mount -t sysfs sysfs /sys
if [ $? -ne 0 ]; then
	echo "[ $(date +"%T.%3N") ] ERROR: mounting sys fail..." > /dev/kmsg
	exec -c /bin/bash
fi
mount -t debugfs none /sys/kernel/debug/
if [ $? -ne 0 ]; then
	echo "[ $(date +"%T.%3N") ] ERROR: mounting debugfs fail..." > /dev/kmsg
	exec -c /bin/bash
fi

# Ready reboot 2 payload
if [ -e "/lib/firmware/reboot_payload.bin" ]; then
	echo 1 > /sys/devices/r2p/default_payload_ready
fi

# Fix permissions to reboot command
echo "echo b > /proc/sysrq-trigger" >> /sbin/reboot
chmod 755 /sbin/reboot

# Set initlogo
echo 4 > /sys/class/graphics/fb0/blank
echo 0 > /sys/class/graphics/fb0/state
dd if=/logo.raw of=/dev/fb0 bs=4096 count=900 status=none
echo 0 > /sys/class/graphics/fb0/blank

echo "[ $(date +"%T.%3N") ] L4T-INITRAMFS START $(date)" > /dev/kmsg
rootdev=$(sed -ne 's/.*root=//;T' -e 's/\s.*$//p' /proc/cmdline)
rootlabel_retries=$(sed -ne 's/.*rootlabel_retries=//;T' -e 's/\s.*$//p' /proc/cmdline)
if [[ -z ${rootlabel_retries} ]]; then rootlabel_retries=1; fi

if [[ $(grep auto_rootdev_disable /proc/cmdline) -eq 0 ]]; then
	rootid=$(sed -e 's/.*pmc_reboot2payload.hekate_config_id=//' -e 's/\s.*$//' /proc/cmdline)

	count=0
	while [ ${count} -lt ${rootlabel_retries} ]
	do
		rootdevtmp=$(blkid -lt PARTLABEL="${rootid}" -o device)
		if [[ -n ${rootdevtmp} ]]; then rootdev=${rootdevtmp}; echo "[ $(date +"%T.%3N") ] Found root fs via PART LABEL" > /dev/kmsg; fi

		rootdevtmp=$(blkid -lt LABEL="${rootid}" -o device);
		if [[ -n ${rootdevtmp} ]]; then rootdev=${rootdevtmp}; echo "[ $(date +"%T.%3N") ] Found root fs via FS LABEL" > /dev/kmsg; fi

		if [[ -n ${rootdevtmp} ]] || [[ ${rootlabel_retries} -eq 1 ]]; then break; fi
		sleep 0.2
		count=$((count + 1))
	done
fi

# Determine if overlays are defined, and mount root accordingly
lowerdirs=$(sed -ne 's/.*lowerdirs=//;T' -e 's/\s.*$//p' /proc/cmdline)
echo "[ $(date +"%T.%3N") ] Root device found: ${rootdev}" > /dev/kmsg
mount_root
echo "[ $(date +"%T.%3N") ] Rootfs mounted over ${rootdev}" > /dev/kmsg
if [ "${lowerdirs}" ]; then
	echo "[ $(date +"%T.%3N") ] Overlays defined: ${lowerdirs}" > /dev/kmsg
    mount_boot
	if [[ -n "${lowerdirs}" ]]; then
		#
		# Prepare overlay mount
		#

		# Create workdir
		mkdir -p /mnt/tmp
		mkdir -p /tmp/.work
		mount -o bind /tmp /mnt/tmp
		# Retrieve elements as an array
		lowerdirs=("$(echo ${lowerdirs} | tr ":" "\n")")

		# Create proper list
		readarray -t lowerdirs <<<"$lowerdirs"

		# Loop over overlays passed to kernel cmdline
		for lowerdir in ${lowerdirs[@]}; do
			if [[ ! -e "/mnt/tmp/boot/${SWR_DIR}/lowerdir/${lowerdir}" ]]; then
				fatal_error "${SWR_DIR}/lowerdir/${lowerdir} does not exist !"
			else
				echo "[ $(date +"%T.%3N") ] Mounting ${lowerdir}" > /dev/kmsg;

				mkdir -p "/${lowerdir}"
				mount -o loop,ro "/tmp/boot/${SWR_DIR}/lowerdir/${lowerdir}" "/${lowerdir}"
				if [ $? -ne 0 ]; then
					fatal_error "Failed to mount ${lowerdir}"
				fi
			fi
		done

		# Retrieve a list of top nested dirs in mounted overlay
		readarray -t lowerdirsPaths <<<"$(find ${lowerdirs[@]/#//} -mindepth 1 -maxdepth 1)"
        overlay_lower_dirs="/mnt:"
		for lowerdir in "${lowerdirs[@]}"
		do
			for lowerdirPath in "${lowerdirsPaths[@]}"
			do
				if [[ "${lowerdirPath}" == *"${lowerdir}"* ]]; then
					overlay_lower_dirs+="/mnt/tmp/boot/${SWR_DIR}/${i}:"
					break
				fi
			done
		done
		overlay_lower_dirs=${overlay_lower_dirs::${#overlay_lower_dirs}-1} # Remove final : in string
	fi
	mount -t overlay overlay -o lowerdir=${overlay_lower_dirs},upperdir=/mnt/tmp/real_root,workdir=/mnt/tmp/.work /mnt
	if [ $? -ne 0 ]; then
		fatal_error "Failed to create/mount OverlayFS"
	else
		echo "[ $(date +"%T.%3N") ] Overlayfs mounted over /mnt" > /dev/kmsg
	fi
fi

resize_root_fs

# Set screen status
#if [[ -f mnt/etc/switchroot_version.conf ]]; then
#	dd if=/logo_mounted.raw of=/dev/fb0 bs=442560 seek=3 status=none
#else
#	fatal_error "ERROR: ${rootdev} not a switchroot installation..."
#fi


mount -o bind /proc /mnt/proc
mount -o bind /sys /mnt/sys
mount -o bind /dev/ /mnt/dev

if [ ! "${lowerdirs}" ]; then
	mount_boot
fi

echo "[ $(date +"%T.%3N") ] Check for dev updates and apply if they exist" > /dev/kmsg
if [[ "${boot_dev_found}" = "true" ]]; then
	SWR_DIR=$(sed -ne 's/.*swr_dir=//;T' -e 's/\s.*$//p' /proc/cmdline)
	if [[ -n ${SWR_DIR} ]]; then
		if [[ -f /tmp/boot/${SWR_DIR}/modules.tar.gz ]]; then
			dd if=/logo_update.raw of=/dev/fb0 bs=442560 seek=3 status=none
			echo "[ $(date +"%T.%3N") ] Deleting old modules" > /dev/kmsg
			rm -r /mnt/lib/modules/*
			echo "[ $(date +"%T.%3N") ] Extracting new modules" > /dev/kmsg
			tar -hxpf /tmp/boot/${SWR_DIR}/modules.tar.gz -C /mnt/lib/
			if [ $? -eq 0 ]; then mount -o remount,rw /tmp/boot; rm /tmp/boot/${SWR_DIR}/modules.tar.gz; mount -o remount,ro /tmp/boot; echo "[ $(date +"%T.%3N") ] Extract success" > /dev/kmsg; fi
				# Fix symlinks for kernel sources.
				ln -s /usr/src/linux-headers-4.9.140-tegra-ubuntu18.04_aarch64/kernel-4.9 /mnt/lib/modules/4.9.140+/build
				ln -s /usr/src/linux-headers-4.9.140-tegra-ubuntu18.04_aarch64/kernel-4.9 /mnt/lib/modules/4.9.140+/source
			fi

			if [[ -f /tmp/boot/${SWR_DIR}/update.tar.gz ]]; then
				dd if=/logo_update.raw of=/dev/fb0 bs=442560 seek=3 status=none
				echo "[ $(date +"%T.%3N") ] Extracting update.tar.gz" > /dev/kmsg
				tar -hxpf /tmp/boot/${SWR_DIR}/update.tar.gz -C /mnt/
				if [ $? -eq 0 ]; then mount -o remount,rw /tmp/boot; rm /tmp/boot/${SWR_DIR}/update.tar.gz; mount -o remount,rw /tmp/boot echo "[ $(date +"%T.%3N") ] Extract success" > /dev/kmsg; fi
			fi
	fi
fi

cd /mnt

if [ $? -ne 0 ]; then
	fatal_error "ERROR: change dir to mnt..."
fi

echo "[ $(date +"%T.%3N") ] Set Up Joycon stuff" > /dev/kmsg
# Create Fallback Bluetooth MAC
BT_MAC_ADDR=$(sed -e "s/^"0x"//" /sys/block/mmcblk0/device/serial)
BT_MAC_ADDR=${BT_MAC_ADDR%??}
BT_MAC_ADDR="98B6E9${BT_MAC_ADDR}"
# Create Wifi MAC Should never change, once set....
NEW_WIFI_MAC=${BT_MAC_ADDR%??}
NEW_WIFI_MAC=${NEW_WIFI_MAC}$(sed -e "s/^"0x"//" /sys/block/mmcblk0/device/serial | grep -o '.\{2\}$')
NEW_WIFI_MAC=$(echo ${NEW_WIFI_MAC} | tr '[:upper:]' '[:lower:]')
NEW_WIFI_MAC=$(echo ${NEW_WIFI_MAC} | sed 's/\(..\)/\1:/g;s/:$//')

if [[ -f /mnt/lib/firmware/brcm/BCM.hcd ]]; then
	echo "[ $(date +"%T.%3N") ] Deleting legacy BCM.hcd" > /dev/kmsg
	rm /mnt/lib/firmware/brcm/BCM.hcd;
fi

if [[ "${boot_dev_found}" = "true" ]]; then
	if [[ -f /tmp/boot/switchroot/joycon_mac.ini ]]; then
		# Collect joycon_00 info from ini
		JOYCON_00_TYPE=$(sed -nr "/^\[joycon_00\]/ { :l /^type[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/switchroot/joycon_mac.ini | tr -d "\\r\\n" )
		JOYCON_00_MAC=$(sed -nr "/^\[joycon_00\]/ { :l /^mac[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/switchroot/joycon_mac.ini | tr -d "\\r\\n")
		JOYCON_00_HOST_MAC=$(sed -nr "/^\[joycon_00\]/ { :l /^host[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/switchroot/joycon_mac.ini | tr -d "\\r\\n")
		JOYCON_00_LTK=$(sed -nr "/^\[joycon_00\]/ { :l /^ltk[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/switchroot/joycon_mac.ini | tr -d "\\r\\n")

		# collect joycon_00 info from ini
		JOYCON_01_TYPE=$(sed -nr "/^\[joycon_01\]/ { :l /^type[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/switchroot/joycon_mac.ini | tr -d "\\r\\n")
		JOYCON_01_MAC=$(sed -nr "/^\[joycon_01\]/ { :l /^mac[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/switchroot/joycon_mac.ini | tr -d "\\r\\n")
		JOYCON_01_HOST_MAC=$(sed -nr "/^\[joycon_01\]/ { :l /^host[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/switchroot/joycon_mac.ini | tr -d "\\r\\n")
		JOYCON_01_LTK=$(sed -nr "/^\[joycon_01\]/ { :l /^ltk[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/switchroot/joycon_mac.ini | tr -d "\\r\\n")
	elif [[ -f /tmp/boot/joycon_mac.ini ]]; then
		# Collect joycon_00 info from ini
		JOYCON_00_TYPE=$(sed -nr "/^\[joycon_00\]/ { :l /^type[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/joycon_mac.ini | tr -d "\\r\\n" )
		JOYCON_00_MAC=$(sed -nr "/^\[joycon_00\]/ { :l /^mac[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/joycon_mac.ini | tr -d "\\r\\n" )
		JOYCON_00_HOST_MAC=$(sed -nr "/^\[joycon_00\]/ { :l /^host[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/joycon_mac.ini | tr -d "\\r\\n" )
		JOYCON_00_LTK=$(sed -nr "/^\[joycon_00\]/ { :l /^ltk[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/joycon_mac.ini | tr -d "\\r\\n" )

		# Collect joycon_00 info from ini
		JOYCON_01_TYPE=$(sed -nr "/^\[joycon_01\]/ { :l /^type[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/joycon_mac.ini | tr -d "\\r\\n" )
		JOYCON_01_MAC=$(sed -nr "/^\[joycon_01\]/ { :l /^mac[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/joycon_mac.ini | tr -d "\\r\\n" )
		JOYCON_01_HOST_MAC=$(sed -nr "/^\[joycon_01\]/ { :l /^host[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/joycon_mac.ini | tr -d "\\r\\n" )
		JOYCON_01_LTK=$(sed -nr "/^\[joycon_01\]/ { :l /^ltk[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /tmp/boot/joycon_mac.ini | tr -d "\\r\\n"  )
	fi

	if [[ -f /tmp/boot/switchroot/joycon_mac.ini ]] || [[ -f /tmp/boot/joycon_mac.ini ]]; then
		# Handle Host BT MAC. Agnostically.
		if [[ ! "${JOYCON_00_TYPE}" = "0" ]]; then
			BT_MAC_ADDR=${JOYCON_00_HOST_MAC}
		elif [[ ! "${JOYCON_01_TYPE}" = "0" ]]; then
			BT_MAC_ADDR=${JOYCON_01_HOST_MAC}
		fi
		# Handle Joycon Pair data.
		if [[ "${JOYCON_00_TYPE}" = "1" ]]; then
			BT_DEV_00_NAME="Joy-Con (L)"
			BT_DEV_00_ID=8198
			BT_DEV_00_SERVICE_RECORDS="0x00000000=356E0900000A000000000900013503191000090004350D350619010009000135031900010900053503191002090006350909656E09006A090100090009350835061901000901000901002510576972656C6573732047616D65706164090101250747616D657061640902003503090100
0x00010000=36017D0900000A000100000900013503191124090004350D350619010009001135031900110900053503191002090006350909656E09006A0901000900093508350619112409010109000D350F350D350619010009001335031900110901002510576972656C6573732047616D65706164090101250747616D6570616409010225084E696E74656E646F090201090111090202080809020308210902042801090205280109020635B035AE082225AA05010905A1010601FF8521092175089530810285300930750895308102853109317508966901810285320932750896690181028533093375089669018102853F05091901291015002501750195108102050109391500250775049501814205097504950181010501093009310933093416000027FFFF00007510950481020601FF85010901750895309102851009107508953091028511091175089530910285120912750895309102C009020735083506090409090100090209280109020A280109020C090C8009020D280009020E2800
0x00010001=358C0900000A000100010900013503191200090004350D35061901000900013503190001090006350909656E09006A09010009000935083506191200090100090100251B576972656C6573732047616D6570616420506E5020536572766572090101250747616D6570616409020009010309020109057E0902020920060902030900010902042801090205090002"
		elif [[ "${JOYCON_00_TYPE}" = "2" ]]; then
			BT_DEV_00_NAME="Joy-Con (R)"
			BT_DEV_00_ID=8199
			BT_DEV_00_SERVICE_RECORDS="0x00000000=356E0900000A000000000900013503191000090004350D350619010009000135031900010900053503191002090006350909656E09006A090100090009350835061901000901000901002510576972656C6573732047616D65706164090101250747616D657061640902003503090100
0x00010000=36017D0900000A000100000900013503191124090004350D350619010009001135031900110900053503191002090006350909656E09006A0901000900093508350619112409010109000D350F350D350619010009001335031900110901002510576972656C6573732047616D65706164090101250747616D6570616409010225084E696E74656E646F090201090111090202080809020308210902042801090205280109020635B035AE082225AA05010905A1010601FF8521092175089530810285300930750895308102853109317508966901810285320932750896690181028533093375089669018102853F05091901291015002501750195108102050109391500250775049501814205097504950181010501093009310933093416000027FFFF00007510950481020601FF85010901750895309102851009107508953091028511091175089530910285120912750895309102C009020735083506090409090100090209280109020A280109020C090C8009020D280009020E2800
0x00010001=358C0900000A000100010900013503191200090004350D35061901000900013503190001090006350909656E09006A09010009000935083506191200090100090100251B576972656C6573732047616D6570616420506E5020536572766572090101250747616D6570616409020009010309020109057E0902020920070902030900010902042801090205090002"
		fi

		if [[ "${JOYCON_01_TYPE}" = "1" ]]; then
			BT_DEV_01_NAME="Joy-Con (L)"
			BT_DEV_01_ID=8198
			BT_DEV_01_SERVICE_RECORDS="0x00000000=356E0900000A000000000900013503191000090004350D350619010009000135031900010900053503191002090006350909656E09006A090100090009350835061901000901000901002510576972656C6573732047616D65706164090101250747616D657061640902003503090100
0x00010000=36017D0900000A000100000900013503191124090004350D350619010009001135031900110900053503191002090006350909656E09006A0901000900093508350619112409010109000D350F350D350619010009001335031900110901002510576972656C6573732047616D65706164090101250747616D6570616409010225084E696E74656E646F090201090111090202080809020308210902042801090205280109020635B035AE082225AA05010905A1010601FF8521092175089530810285300930750895308102853109317508966901810285320932750896690181028533093375089669018102853F05091901291015002501750195108102050109391500250775049501814205097504950181010501093009310933093416000027FFFF00007510950481020601FF85010901750895309102851009107508953091028511091175089530910285120912750895309102C009020735083506090409090100090209280109020A280109020C090C8009020D280009020E2800
0x00010001=358C0900000A000100010900013503191200090004350D35061901000900013503190001090006350909656E09006A09010009000935083506191200090100090100251B576972656C6573732047616D6570616420506E5020536572766572090101250747616D6570616409020009010309020109057E0902020920060902030900010902042801090205090002"
		elif [[ ${JOYCON_01_TYPE} == "2" ]]; then
			BT_DEV_01_NAME="Joy-Con (R)"
			BT_DEV_01_ID=8199
			BT_DEV_01_SERVICE_RECORDS="0x00000000=356E0900000A000000000900013503191000090004350D350619010009000135031900010900053503191002090006350909656E09006A090100090009350835061901000901000901002510576972656C6573732047616D65706164090101250747616D657061640902003503090100
0x00010000=36017D0900000A000100000900013503191124090004350D350619010009001135031900110900053503191002090006350909656E09006A0901000900093508350619112409010109000D350F350D350619010009001335031900110901002510576972656C6573732047616D65706164090101250747616D6570616409010225084E696E74656E646F090201090111090202080809020308210902042801090205280109020635B035AE082225AA05010905A1010601FF8521092175089530810285300930750895308102853109317508966901810285320932750896690181028533093375089669018102853F05091901291015002501750195108102050109391500250775049501814205097504950181010501093009310933093416000027FFFF00007510950481020601FF85010901750895309102851009107508953091028511091175089530910285120912750895309102C009020735083506090409090100090209280109020A280109020C090C8009020D280009020E2800
0x00010001=358C0900000A000100010900013503191200090004350D35061901000900013503190001090006350909656E09006A09010009000935083506191200090100090100251B576972656C6573732047616D6570616420506E5020536572766572090101250747616D6570616409020009010309020109057E0902020920070902030900010902042801090205090002"
		fi
		if [[ ! -d /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/cache ]]; then
			mkdir -p /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/cache
		fi

		if [[ ! "${JOYCON_00_TYPE}" = "0" ]]; then
			if [[ ! -d /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/${JOYCON_00_MAC} ]]; then
				mkdir -p /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/${JOYCON_00_MAC}
				cat << EOF > /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/${JOYCON_00_MAC}/info
[General]
Name=${BT_DEV_00_NAME}
Class=0x000508
SupportedTechnologies=BR/EDR;
Trusted=true
Blocked=false
Services=00001000-0000-1000-8000-00805f9b34fb;00001124-0000-1000-8000-00805f9b34fb;00001200-0000-1000-8000-00805f9b34fb;
[LinkKey]
Key=${JOYCON_00_LTK}
Type=4
PINLength=0

[DeviceID]
Source=2
Vendor=1406
Product=${BT_DEV_00_ID}
Version=1

[ConnectionParameters]
MinInterval=5
MaxInterval=15
Latency=120
Timeout=600
EOF
			cat << EOF > /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/cache/${JOYCON_00_MAC}
[General]
Name=${BT_DEV_00_NAME}

[ServiceRecords]
${BT_DEV_00_SERVICE_RECORDS}
EOF
			fi
		fi

		if [[ ! "${JOYCON_01_TYPE}" = "0" ]]; then
			if [[ ! -d /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/${JOYCON_01_MAC} ]]; then
				mkdir -p /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/${JOYCON_01_MAC}
				cat << EOF > /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/${JOYCON_01_MAC}/info
[General]
Name=${BT_DEV_01_NAME}
Class=0x000508
SupportedTechnologies=BR/EDR;
Trusted=true
Blocked=false
Services=00001000-0000-1000-8000-00805f9b34fb;00001124-0000-1000-8000-00805f9b34fb;00001200-0000-1000-8000-00805f9b34fb;
[LinkKey]
Key=${JOYCON_01_LTK}
Type=4
PINLength=0

[DeviceID]
Source=2
Vendor=1406
Product=${BT_DEV_01_ID}
Version=1

[ConnectionParameters]
MinInterval=5
MaxInterval=15
Latency=120
Timeout=600
EOF
			cat << EOF > /mnt/var/lib/bluetooth/${BT_MAC_ADDR}/cache/${JOYCON_01_MAC}
[General]
Name=${BT_DEV_01_NAME}

[ServiceRecords]
${BT_DEV_01_SERVICE_RECORDS}
EOF

			fi
		fi
	fi
fi

# Set btmac in firmware
if [[ ${BT_MAC_ADDR} == *":"* ]]; then
  BT_MAC_ADDR=$( echo ${BT_MAC_ADDR} | tr -d ':' )
fi
BT_MAC_ADDR=${BT_MAC_ADDR^^}

# Convert to little endian for firmware comparison and injection....
i=${#BT_MAC_ADDR}

while [ $i -gt 0 ]
do
	i=$((i-2))
	CONVERTED_MAC=${CONVERTED_MAC}$( echo -n "${BT_MAC_ADDR:$i:2}" )
done
NEW_BT_MAC=${CONVERTED_MAC};
CONVERTED_MAC=$( echo $CONVERTED_MAC | sed 's/../\\x&/g' )
if [[ -f /mnt/lib/firmware/brcm/BCM4356A3.hcd ]]; then
   # Dump current mac to variable
   CURRENT_BT_MAC=$(/bin/xxd -p  -s 0x21 -l 6 -u /mnt/lib/firmware/brcm/BCM4356A3.hcd)
   if [[ ! ${CURRENT_BT_MAC} == ${NEW_BT_MAC} ]]; then
	   # Write converted_mac to firmware file at 0x21 - 0x26
	   printf $CONVERTED_MAC | dd of=/mnt/lib/firmware/brcm/BCM4356A3.hcd bs=1 seek=33 count=6 conv=notrunc
	   echo "[ $(date +"%T.%3N") ] BD address patched" > /dev/kmsg
   fi
fi

# Patch wifi mac address
if [[ -f /mnt/lib/firmware/brcm/brcmfmac4356-pcie.txt ]]; then
	CURRENT_WIFI_MAC=$(sed -nr "{ :l /^macaddr[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /mnt/lib/firmware/brcm/brcmfmac4356-pcie.txt)
	if [[ ! ${CURRENT_WIFI_MAC} == ${NEW_WIFI_MAC} ]]; then
		#Patch WIFI Mac
		sed -i 's/macaddr=.*/macaddr='${NEW_WIFI_MAC}'/' /mnt/lib/firmware/brcm/brcmfmac4356-pcie.txt
		echo "[ $(date +"%T.%3N") ] WIFI address patched" > /dev/kmsg
	fi
fi

# Patch bluez config to enable fast connect, required or joycon wont connect
sed -i 's/#FastConnectable = false/FastConnectable = true/' /mnt/etc/bluetooth/main.conf

# Patch turul with ports in apt sources list 
sed -E -i 's/turul.canonical/ports.ubuntu/g' /mnt/etc/apt/sources.list

# Init ZRAM with 1024MB total
echo lz4 > /sys/block/zram0/comp_algorithm
echo 1024M > /sys/block/zram0/disksize
/sbin/mkswap /dev/zram0
/sbin/swapon -p 5 /dev/zram0

# Fix permissions for nvpmodel
chmod 0666 /sys/devices/thermal-fan-est/coeff
chmod 0666 /sys/devices/70090000.xusb/downgrade_usb3
chmod 0666 /sys/class/power_supply/usb/charge_control_limit

# Add auto profiles config if missing
if [ ! -e "/mnt/var/lib/nvpmodel/auto_profiles" ]; then
	echo 1 > /mnt/var/lib/nvpmodel/auto_profiles
	chmod 0666 /mnt/var/lib/nvpmodel/auto_profiles
fi

# Add charging limits config if missing, otherwise set it.
if [ ! -e "/mnt/var/lib/nvpmodel/charging_status" ]; then
	echo 0 > /mnt/var/lib/nvpmodel/charging_status
	chmod 0666 /mnt/var/lib/nvpmodel/charging_status
else
	echo $(cat /mnt/var/lib/nvpmodel/charging_status) > /sys/class/power_supply/usb/charge_control_limit
fi

if [ ! "${lowerdirs}" ]; then
	umount /tmp/boot
fi

# Set initlogo
dd if=/logo_booting.raw of=/dev/fb0 bs=442560 seek=3 status=none

echo "[ $(date +"%T.%3N") ] Switching from initrd to actual rootfs" > /dev/kmsg
init_path="/sbin/init"
if [[ -L sbin/init ]]; then
	# sed absolutely needs double quotes here
	init_path=$(ls -l sbin/init | sed -n "s/.*\\-> \(.*\)/\1/p")
	echo "[ $(date +"%T.%3N") ] Found symlinked init: ${init_path}!" > /dev/kmsg
fi

echo "[ $(date +"%T.%3N") ] L4T-INITRAMFS END $(date)" > /dev/kmsg
exec chroot . ${init_path} ${runlevel}
